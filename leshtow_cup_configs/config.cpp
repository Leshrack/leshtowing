class CfgPatches 
{
	class lesh_tow_cup 
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"CUP_AirVehicles_A10","CUP_AirVehciles_AH64","CUP_AirVehicles_AV8B","CUP_AirVehciles_Merlin","CUP_AirVehciles_AW159","CUP_AirVehicles_C130J","CUP_AirVehicles_DC3","CUP_AirVehicles_CH47","CUP_AirVehicles_CH53E","CUP_AirVehicles_Ka50","CUP_AirVehicles_MH60S","CUP_AirVehicles_Mi8","CUP_AirVehicles_Mi24","CUP_AirVehicles_MV22","CUP_AirVehicles_Su25","CUP_AirVehicles_UH60"};
	};
};

class CfgVehicles 	
{
	/*extern*/ class CUP_AH64_base;
	/*extern*/ class CUP_C47_Base;
	/*extern*/ class CUP_DC3_Base;
	/*extern*/ class Helicopter_Base_F;
	/*extern*/ class Helicopter_Base_H;
	/*extern*/ class Plane;
	/*extern*/ class Plane_Base_F;

	//CUP_B_A10_AT_USA,CUP_B_A10_CAS_USA
	class CUP_A10_Base : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7.5,0.19};
		LESH_WheelOffset[] = {0,-1};
	};

	//CUP_B_AH64D_USA,CUP_B_AH64D_AT_USA,CUP_B_AH64D_NO_USA,CUP_B_AH64D_ES_USA,CUP_B_AH64D_MR_USA
	//CUP_B_AH1_BAF,CUP_B_AH1_AT_BAF,CUP_B_AH1_NO_BAF,CUP_B_AH1_ES_BAF,CUP_B_AH1_MR_BAF
	class CUP_AH64D_Base : CUP_AH64_base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8.8,1};
		LESH_WheelOffset[] = {0,2.5};
	};

	//CUP_B_AV8B_Deepstrike,CUP_B_AV8B_Empty,CUP_B_AV8B_FFAR_19,CUP_B_AV8B_FFAR_7,CUP_B_AV8B_Heavy,CUP_B_AV8B_LGB,CUP_B_AV8B_Hydra19,CUP_B_AV8B
	class CUP_AV8B_Base : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,4.5,-1.1};
		LESH_WheelOffset[] = {0,-4.5};
	};

	//CUP_Merlin_HC3,CUP_Merlin_HC3_FFV,CUP_Merlin_HC3_MED
	class CUP_Merlin_HC3_FFV : Helicopter_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9.5,-2.2};
		LESH_WheelOffset[] = {0,0};
	};

	//CUP_B_AW159_Armed_BAF,CUP_B_AW159_Unarmed_BAF
	class CUP_AW159_Unarmed_Base : Helicopter_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,6.5,-0.8};
		LESH_WheelOffset[] = {0,0.4};
	};

	//CUP_B_C130J_USMC
	class CUP_C130J_Base : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,13,-4.55};
		LESH_WheelOffset[] = {0,-1};
	};

	//CUP_C_C47_CIV
	class CUP_C_C47_CIV : CUP_C47_Base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-10.5,0};
		LESH_WheelOffset[] = {0,4.2};
	};

	//CUP_C_DC3_CIV
	class CUP_C_DC3_CIV : CUP_DC3_Base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-10.5,0};
		LESH_WheelOffset[] = {0,4.2};
	};

	//CUP_B_CH47F_USA,CUP_B_CH47F_GB
	class CUP_CH47F_base : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-7.5,-2.36};
		LESH_WheelOffset[] = {0,3};
	};

	//CUP_B_CH53E_USMC
	class CUP_B_CH53E_USMC : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7,-1.1};
		LESH_WheelOffset[] = {0,-3.6};
	};

	//CUP_O_Ka50_AA_SLA,CUP_O_Ka50_AA_SLA
	class CUP_KA50_Base : Helicopter_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7,1.03};
		LESH_WheelOffset[] = {0,-1};
	};

	//CUP_B_MH60S_USMC,CUP_B_MH60S_FFV_USMC
	class CUP_MH60S_Base : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8.2,-0.96};
		LESH_WheelOffset[] = {0,3};
	};

	//CUP_B_Mi17_CDF,CUP_O_Mi17_TK,CUP_I_Mi17_UN,CUP_C_Mi17_Civilian_RU,CUP_B_Mi171Sh_ACR,CUP_B_Mi171Sh_ACR
	//CUP_O_Mi8_CHDKZ,CUP_O_Mi8_SLA_1,CUP_O_Mi8_RU,CUP_O_Mi8_SLA_2
	class CUP_Mi8_base : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7.5,-2.355};
		LESH_WheelOffset[] = {0,0};
	};

	//CUP_B_Mi24_D_CDF,CUP_I_Mi24_D_ION,CUP_O_Mi24_D_TK,CUP_I_Mi24_D_UN,CUP_O_Mi24_P_RU,CUP_O_Mi24_V_RU
	class CUP_Mi24_Base : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9,-1.89};
		LESH_WheelOffset[] = {0,1};
	};

	//CUP_B_Mi35_CZ
	class CUP_Mi35_Base : CUP_Mi24_Base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9,-1.89};
		LESH_WheelOffset[] = {0,1};
	};

	//CUP_B_MV22_USMC
	class CUP_B_MV22_USMC : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7.8,-2.37};
		LESH_WheelOffset[] = {0,-1.5};
	};

	//CUP_B_Su25_CDF,CUP_O_Su25_RU_3,CUP_O_Su25_SLA,CUP_O_Su25_TKA,CUP_O_Su25_RU_1,CUP_O_Su25_RU_2
	class CUP_Su25_base : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7,-1.68};
		LESH_WheelOffset[] = {0,-1};
	};

	//CUP_I_UH60L_RACS,CUP_B_UH60L_US,CUP_I_UH60L_FFV_RACS,CUP_B_UH60L_FFV_US,CUP_I_UH60L_Unarmed_RACS,CUP_B_UH60L_Unarmed_US,CUP_I_UH60L_Unarmed_FFV_Racs,CUP_B_UH60L_Unarmed_FFV_US
	//CUP_B_UH60M_US,CUP_B_UH60M_FFV_US,CUP_B_UH60M_Unarmed_US,CUP_B_UH60M_Unarmed_FFV_US
	class CUP_Uh60_Base : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8.2,-1.62};
		LESH_WheelOffset[] = {0,3};
	};

};


