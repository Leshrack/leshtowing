class CfgPatches 
{
	class lesh_tow_jsjcf18x
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"JS_S_FA18X"};
	};
};

class CfgVehicles 	
{
	/*extern*/ class Plane;
	class JS_S_FA18X : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9,-1.15};
		LESH_WheelOffset[] = {0,-4};
	};
};