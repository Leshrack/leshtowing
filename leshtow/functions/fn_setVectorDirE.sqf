/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_setVectorDirE

    FEATURE
    Rotates the plane around the configured rear axis value instead of model center

    USAGE
    [_target, _vectorDir, _center] call LESH_towing_fnc_setVectorDirE;

    PARAMETERS
    1. Object - a vehicle   | MANDATORY
    2. Vector - the new direction for the target object | MANDATORY
    3. Position - the prefferd position around which the rotation should be performed | MANDATORY
*/

params [["_object", objNull], ["_vectorDir", [0,0,0]], ["_center", [0,0,0]]];

// get the current coordinates for the point around we wish to rotate
_currentPositionCenter = _object modelToWorldVisual _center;

// rotate the object
_object setVectorDir _vectorDir;

// get the new position of the center point (it will have changed unless the center point was [0,0,0])
_newPositionCenter = _object modelToWorldVisual _center;

// calculate the drift between the new and old centerpoints
_deltaPositionCenter = _currentPositionCenter vectorDiff _newPositionCenter;

// compensate by adjusting the position with the drift
_object setPosWorld ((getPosWorld _object) vectorAdd _deltaPositionCenter);

true
