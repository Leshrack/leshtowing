class CfgPatches 
{
	class lesh_tow_vanilla 
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"A3_Air_F_Beta_Heli_Attack_01","A3_Air_F_Beta_Heli_Attack_02","A3_Air_F_Beta_Heli_Transport_01","A3_Air_F_Beta_Heli_Transport_02","A3_Air_F_EPB_Heli_Light_03","A3_Air_F_EPC_Plane_CAS_01","A3_Air_F_EPC_Plane_CAS_02","A3_Air_F_EPC_Plane_Fighter_03","A3_Air_F_Exp_Heli_Light_01","A3_Air_F_Exp_Heli_Transport_01","A3_Air_F_Exp_Plane_Civil_01","A3_Air_F_Exp_UAV_03","A3_Air_F_Exp_UAV_04","A3_Air_F_Exp_VTOL_01","A3_Air_F_Exp_VTOL_02","A3_Air_F_Gamma_Plane_Fighter_03","A3_Air_F_Gamma_UAV_01","A3_Air_F_Gamma_UAV_02","A3_Air_F_Heli_Heli_Attack_01","A3_Air_F_Heli_Heli_Attack_02","A3_Air_F_Heli_Heli_Light_01","A3_Air_F_Heli_Heli_Light_02","A3_Air_F_Heli_Heli_Light_03","A3_Air_F_Heli_Heli_Transport_01","A3_Air_F_Heli_Heli_Transport_02","A3_Air_F_Heli_Heli_Transport_03","A3_Air_F_Heli_Heli_Transport_04","A3_Air_F_Heli_Light_01","A3_Air_F_Heli_Light_02","A3_Air_F_Jets_Plane_Fighter_01","A3_Air_F_Jets_Plane_Fighter_02","A3_Air_F_Jets_Plane_Fighter_04","A3_Air_F_Jets_UAV_05","A3_Air_F_Orange_Heli_Transport_02","A3_Air_F_Orange_UAV_01","A3_Air_F_Orange_UAV_06"};
	};
};

class CfgVehicles
{
	/*extern*/ class Car_F;
	class UGV_01_base_F : Car_F 
	{
		LESH_canTow = 1;
		LESH_AxisOffsetTower[] = {0.43,-3,-0.94};
	};

	/*extern*/ class Plane_Base_F;
	class Plane_CAS_01_base_F : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7.5,-1.3};
		LESH_WheelOffset[] = {0,-0.3};
	};
	
	class Plane_Fighter_01_Base_F : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9,-1.2};
		LESH_WheelOffset[] = {0,-4};
	};
	
	class Plane_Fighter_02_Base_F : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,11,-1};
		LESH_WheelOffset[] = {0,-4.5};
	};

	class Plane_Fighter_03_base_F : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,6.8,-1.4};
		LESH_WheelOffset[] = {0,-1.1};
	};

	class Plane_Fighter_04_Base_F : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7.5,-1.4};
		LESH_WheelOffset[] = {0,-1.5};
	};

	class Plane_CAS_02_base_F : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,8.3,-1.65};
		LESH_WheelOffset[] = {0,-2.3};
	};

	class Plane_Civil_01_base_F : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,4.5,-0.5};
		LESH_WheelOffset[] = {0,0.5};
	};

	/*extern*/ class Helicopter_Base_F;
	class Heli_Attack_01_base_F : Helicopter_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-7.3,-0.95};
		LESH_WheelOffset[] = {0,2.5};
	};

	class Heli_Attack_02_base_F : Helicopter_Base_F 
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8,-2};
		LESH_WheelOffset[] = {0,2.4};
	};

	/*extern*/ class Helicopter_Base_H;
	class Heli_Transport_01_base_F : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-9.2,-1};
		LESH_WheelOffset[] = {0,4.0};
	};

	class Heli_Transport_02_base_F : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,8.7,-2.57};
		LESH_WheelOffset[] = {0,-0.3};
	};

	class Heli_Transport_03_base_F : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-6.2,-2.2};
		LESH_WheelOffset[] = {0,2.4};
	};

	class Heli_Transport_04_base_F : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,6,-1.78};
		LESH_WheelOffset[] = {0,-1};
	};

	class Heli_Light_02_base_F : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8,-0.75};
		LESH_WheelOffset[] = {0,2.5};
	};

	/*extern*/ class VTOL_Base_F;
	class VTOL_01_base_F: VTOL_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,13,-5.66};
		LESH_WheelOffset[] = {0,0};
	};
	class VTOL_02_base_F: VTOL_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,8,-1.9};
		LESH_WheelOffset[] = {0,-2};
	};

	/*extern*/ class UAV;
	class UAV_02_Base_F: UAV 
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,3.5,-0.6};
		LESH_WheelOffset[] = {0,-2.6};
	};

	class UAV_03_base_F: Helicopter_Base_F 
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-6.2,-0.4};
		LESH_WheelOffset[] = {0,0.9};
	};

	class UAV_04_base_F: UAV 
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,2.7,-0.6};
		LESH_WheelOffset[] = {0,-0.8};
	};

	class UAV_05_Base_F: UAV 
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,4.5,-0.8};
		LESH_WheelOffset[] = {0,-1.5};
	};
};
