class CfgPatches 
{
	class lesh_tow_rhs
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"rhs_c_a2port_air","RHS_US_A2_AirImport","rhs_c_a3retex","rhs_cti_insurgents"};
	};
};

class CfgVehicles 	
{	

	//RHS_A10
	/*extern*/ class Plane_CAS_01_Base_F;
	class RHS_A10 : Plane_CAS_01_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,8,0.27};
		LESH_WheelOffset[] = {0,0};
	};

	//RHS_AH64D,RHS_AH64D_AA,RHS_AH64D_CS,RHS_AH64D_GS,RHS_AH64D_wd,RHS_AH64D_wd_AA,RHS_AH64D_wd_CS,RHS_AH64D_wd_GS,RHS_AH64DGrey
	/*extern*/ class RHS_AH64_base;
	class RHS_AH64D : RHS_AH64_base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8.3,-1};
		LESH_WheelOffset[] = {0,2.5};
	};

	//RHS_C130J
	/*extern*/ class RHS_C130J_Base;
	class RHS_C130J : RHS_C130J_Base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,15,1.17};
		LESH_WheelOffset[] = {0,2};
	};

	//RHS_CH_47F,RHS_CH_47F_10,RHS_CH_47F_light
	/*extern*/ class RHS_CH_47F_base;
	class RHS_CH_47F : RHS_CH_47F_base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8,-2.5};
		LESH_WheelOffset[] = {0,3};
	};

	//RHS_Ka52_UPK23_vvs,RHS_Ka52_UPK23_vvsc,RHS_Ka52_vvs,RHS_Ka52_vvsc
	/*extern*/ class Heli_Attack_02_base_F;
	class RHS_Ka52_base : Heli_Attack_02_base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,6.5,-1.05};
		LESH_WheelOffset[] = {0,-1};
	};

	//rhs_ka60_c,rhs_ka60_grey
	/*extern*/ class O_Heli_Light_02_unarmed_F;
	class rhs_ka60_grey :O_Heli_Light_02_unarmed_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8,-0.74};
		LESH_WheelOffset[] = {0,3};
	};

	//RHS_Mi24P_AT_vdv,RHS_Mi24P_AT_vvs,RHS_Mi24P_AT_vvsc,RHS_Mi24P_CAS_vdv,RHS_Mi24P_CAS_vvs,RHS_Mi24P_CAS_vvsc,RHS_Mi24P_vdv,RHS_Mi24P_vvs,RHS_Mi24P_vvsc
	/*extern*/ class RHS_Mi24_base;
	class RHS_Mi24P_VVS_Base : RHS_Mi24_base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0.5,8.5,-2.21};
		//LESH_customDirection = -4;
		LESH_WheelOffset[] = {0.5,1};
	};

	//RHS_Mi24V_AT_vdv,RHS_Mi24V_AT_vvs,RHS_Mi24V_AT_vvsc,RHS_Mi24V_FAB_vdv,RHS_Mi24V_FAB_vvs,RHS_Mi24V_FAB_vvsc,RHS_Mi24V_UPK23_vdv,RHS_Mi24V_UPK23_vvs,RHS_Mi24V_UPK23_vvsc,RHS_Mi24V_vdv,RHS_Mi24V_vvs,RHS_Mi24V_vvsc
	/*extern*/// class RHS_Mi24_base;
	class RHS_Mi24V_Base : RHS_Mi24_base 
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {-0.12,8.5,-2.21};
		LESH_WheelOffset[] = {-0.12,1};
	};

	//RHS_Mi24Vt_vvs
	/*extern*/ class RHS_Mi24V_vvs;
	class RHS_Mi24Vt_vvs : RHS_Mi24V_vvs
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {-0.12,8.5,-2.21};
		LESH_WheelOffset[] = {-0.12,1};
	};

	/*extern*/ class RHS_Mi8_base;
	//RHS_Mi8mt_Cargo_vdv,RHS_Mi8mt_Cargo_vv,RHS_Mi8mt_Cargo_vvs,RHS_Mi8mt_Cargo_vvsc,RHS_Mi8mt_vdv,RHS_Mi8mt_vv,RHS_Mi8mt_vvs,RHS_Mi8mt_vvsc
	class RHS_Mi8_VVS_Base : RHS_Mi8_base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7.5,-2.40};
		LESH_WheelOffset[] = {0,0.6};
	};

	//RHS_Mi8amt_chdkz,RHS_Mi8amt_civilian,RHS_Mi8AMT_vdv,RHS_Mi8AMT_vvs,RHS_Mi8AMT_vvsc
	class rhs_mi8amt_base : RHS_Mi8_base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7.5,-2.40};
		LESH_WheelOffset[] = {0,0.6};
	};

	//RHS_Mi8MTV3_FAB_vdv,RHS_Mi8MTV3_FAB_vvs,RHS_Mi8MTV3_FAB_vvsc,RHS_Mi8MTV3_UPK23_vdv,RHS_Mi8MTV3_UPK23_vvs,RHS_Mi8MTV3_UPK23_vvsc,RHS_Mi8MTV3_vdv,RHS_Mi8MTV3_vvs,RHS_Mi8MTV3_vvsc
	class rhs_mi8mtv3_base : RHS_Mi8_base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7.5,-2.40};
		LESH_WheelOffset[] = {0,0.6};
	};

	//RHS_Mi8AMTSh_FAB_vvs,RHS_Mi8AMTSh_FAB_vvsc,RHS_Mi8AMTSh_UPK23_vvs,RHS_Mi8AMTSh_UPK23_vvsc,RHS_Mi8AMTSh_vvs,RHS_Mi8AMTSh_vvsc
	class rhs_mi8amtsh_base : rhs_mi8mtv3_base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7.5,-2.40};
		LESH_WheelOffset[] = {0,0.6};
	};

	//RHS_Su25SM_KH29_vvs,RHS_Su25SM_KH29_vvsc,RHS_Su25SM_vvs,RHS_Su25SM_vvsc
	/*extern*/ class O_Plane_CAS_02_F;
	class RHS_su25_base : O_Plane_CAS_02_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,6.8,-2.04};
		LESH_WheelOffset[] = {0,-0.7};
	};

	//RHS_UH60M,RHS_UH60M_d
	/*extern*/ class RHS_UH60_Base;
	class RHS_UH60M_base : RHS_UH60_Base
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8,-1.62};
		LESH_WheelOffset[] = {0,3};
	};

	//RHS_UH60M_MEV,RHS_UH60M_MEV2,RHS_UH60M_MEV2_d,RHS_UH60M_MEV_d
	/*extern*/ class RHS_UH60M;
	class RHS_UH60M_MEV : RHS_UH60M
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8,-1.62};
		LESH_WheelOffset[] = {0,3};		
	};

};
