class CfgPatches 
{
	class lesh_tow_jsjcf18
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"JS_JC_FA18"};
	};
};

class CfgVehicles 	
{
	/*extern*/ class Plane;
	class JS_JC_FA18E : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9,-1.15};
		LESH_WheelOffset[] = {0,-4};
	};
	class JS_JC_FA18F : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9,-1.15};
		LESH_WheelOffset[] = {0,-4};
	};
};