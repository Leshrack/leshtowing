class CfgPatches 
{
	class lesh_tow_fwf2a
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"FIR_F2A_F"};
	};
};

class CfgVehicles
{
	/*extern*/ class Plane_Fighter_03_base_F;
	class FIR_F2A_Base : Plane_Fighter_03_base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,8,-0.63};
		LESH_WheelOffset[] = {0,-2};
	};
};