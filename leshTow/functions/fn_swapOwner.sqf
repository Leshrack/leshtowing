/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_swapOwner

    FEATURE
    Retrieves a config value from vehicle type

    USAGE
    [_vehicle, _playerObject] call LESH_towing_fnc_swapOwner

    PARAMETERS
    1. Object - the vehicle which we want to transfer to a new owner            | MANDATORY
    2. Object - instance of the player object who is going to be the new owner  | MANDATORY

*/


params [["_unit", objNull],["_owner",objNull]];

private ["_ownerId"];

if ( !isServer ) exitWith { false };

_ownerId = owner _owner;

_success = _unit setOwner _ownerId;

if (_success) exitWith {true};

false;