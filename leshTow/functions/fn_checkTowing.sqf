/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_checkTowing

    FEATURE
    Checks if the towing vehicle is currently towing a vehicle

    USAGE
    [_tower] call LESH_towing_fnc_checkTowing;

    PARAMETERS
    1. Object - the Towing vehicle      | MANDATORY

*/

params [["_tower", objNull]];

if ( isNull _tower ) exitWith { false };

if ( (_tower getVariable ["LESH_towing", false]) ) exitWith { true };

false;
