/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_getTower

    FEATURE
    Retrieves a config value from vehicle type

    USAGE
    [_vehicle, _configProperty] call LESH_towing_fnc_getTower

    PARAMETERS
    1. Object - the vehicle from which we want to extract the config value      | MANDATORY
    2. String - The name of the config property to extract  | MANDATORY

*/

params [["_unit", objNull]];

private ["_tower", "_uav"];

_tower = objNull;

//get connected UAV for player
_uav = getConnectedUAV _unit;

//if the player isn't connected to UAV can't attach anything
if ( isNull _uav ) then {
    //is not connected to uav so must be driving a vehicle lets check
    //if not then there is nothing to tow with so exit out of the checks
    if ( vehicle _unit == _unit ) exitWith {_tower = objNull; _tower };
    if ( driver (vehicle _unit) != _unit ) exitWith { _tower = objNull; _tower };

    //so the player is a driver of a vehicle, lets use that as tower for now
    _tower = (vehicle _unit);
} else {
    //uav might be the tower as the player is connected to one
    _tower = _uav;
};

_tower