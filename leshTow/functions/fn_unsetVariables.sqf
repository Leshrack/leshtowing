/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_unsetVariables

    FEATURE
    Resets any variables applied to vehicles by the towing script to nil

    USAGE
    [unit] call LESH_towing_fnc_unsetVariables;

    PARAMETERS
    1. Object - a vehicle   | MANDATORY
*/

params [["_unit", objNull], ["_type", ""]];

if ( _type == "_tower" ) then {
    _unit setVariable ["LESH_limiter", nil, true];
    _unit setVariable ["LESH_instance", nil, true];
    _unit setVariable ["LESH_towTarget", nil, true];
    _unit setVariable ["LESH_towing", nil, true];
    _unit setVariable ["LESH_tower_killedEHidx", nil, true];
    _unit setVariable ["LESH_toao", nil, true];
};

if ( _type == "_target" ) then {
    _unit setVariable ["LESH_towedBy", nil, true];
    _unit setVariable ["LESH_target_killedEHidx", nil, true];
    _unit setVariable ["LESH_taao", nil, true];
    _unit setVariable ["LESH_two", nil, true];
    _unit setVariable ["LESH_ttff", nil, true];
    _unit setVariable ["LESH_tcd", nil, true];
};

true
