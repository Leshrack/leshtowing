/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_Detach

    FEATURE
    Detaches the towing vehicle from the towed vehicle and effectively ends the towing action

    USAGE
    [_tower] call LESH_towing_fnc_Detach;

    PARAMETERS
    1. Object - the Towing vehicle      | MANDATORY

*/

params [["_tower", objNull]];

private ["_target","_target_kEH","_tower_kEH"];

_target = _tower getVariable ["LESH_towTarget", objNull];

_instance = _tower getVariable ["LESH_instance", 0];

[format["LESH_towing_%1", _instance], "onEachFrame"] call BIS_fnc_removeStackedEventHandler;

[_tower, _instance] remoteExecCall ["LESH_towing_fnc_stopSpeedLimiterOnClient", 2, false];

_target_kEH = _target getVariable "LESH_target_killedEHidx";
_tower_kEH = _tower getVariable "LESH_tower_killedEHidx";

_target removeEventHandler ["killed", _target_kEH];
_tower removeEventHandler ["killed", _tower_kEH];

[_target, "_target"] call LESH_towing_fnc_unsetVariables;
[_tower, "_tower"] call LESH_towing_fnc_unsetVariables;

true;