/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_toggleSpeedLimiter

    FEATURE
    Toggles the towers speed limiter on or off depending on previous setting;

    USAGE
    [] call LESH_towing_fnc_toggleSpeedLimiter;


    PARAMETERS

*/

private ["_tower","_isTowing","_enabled","_instance"];

_tower = [player] call LESH_towing_fnc_getTower;

if ( _tower == objNull ) exitWith { false };

_isTowing = [_tower] call LESH_towing_fnc_checkTowing;

if ( _isTowing ) then {
    _enabled = _tower getVariable ["LESH_limiter", 0];
    _instance = _tower getVariable ["LESH_instance", 0];
    if ( _enabled == 0 ) then {
        [_tower, _instance] call LESH_towing_fnc_startSpeedLimiter;
    } else {
        [_tower, _instance] call LESH_towing_fnc_stopSpeedLimiter;
    };
};

true;
