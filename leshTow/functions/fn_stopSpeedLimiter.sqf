/*
    Author: Leshrack
    Last modified: 20180610
    Function: LESH_towing_fnc_stopSpeedLimiter

    FEATURE

    USAGE
    [] call LESH_towing_fnc_stopSpeedLimiter;


    PARAMETERS

*/

params [["_object", objNull], ["_instance", 0]];

private ["_enabled"];

if ( _object == objNull ) exitWith { false };

_enabled = _tower getVariable ["LESH_limiter", 0];

if (_enabled == 0) exitWith { false; };

_object setVariable ["LESH_limiter", 0];
[format["LESH_speedLimiter_%1", _instance], "onEachFrame"] call BIS_fnc_removeStackedEventHandler;

true;
