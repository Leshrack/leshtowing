/*
    Author: Leshrack
    Last modified: 20180610
    Function: LESH_towing_fnc_removeDebug

    FEATURE
    Removes the visual aids added by the debug function

    USAGE
    [_unit] call LESH_towing_fnc_removeDebug

    PARAMETERS
    1. Object - the vehicle to remove the debug points from | MANDATORY
*/

params [["_unit", objNull]];

_balls = _unit getVariable ["LESH_debugballs", []];

{
    detach _x;
    deleteVehicle _x;
} foreach _balls;

true;
