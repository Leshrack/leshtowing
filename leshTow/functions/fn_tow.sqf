/*
	Author: Leshrack
	Last modified: 20180602
	Function: LESH_towing_fnc_tow

	FEATURE
	Initiates towing between tower and towed vehicle;

	USAGE
	[TowVehicle, TowedVehicle] call LESH_towing_fnc_tow;


	PARAMETERS
	1. Object - Towing vehicle		| MANDATORY
	2. Object - Towable vehicle		| MANDATORY

*/

params [["_tower", objNull], ["_target", objNull]];

private ["_tower_axis_offset","_target_axis_offset","_target_wheel_offset","_targetTowFromFront","_tower_EHkilledIdx","_target_EHkilledIdx","_eventId","_eventId2", "_targetCustomDirection"];

_tower_axis_offset = [_tower, "tower"] call LESH_towing_fnc_getAxisOffset;
_target_axis_offset = [_target, "target"] call LESH_towing_fnc_getAxisOffset;
_target_wheel_offset = [_target] call LESH_towing_fnc_getWheelOffset;
_targetTowFromFront = [_target, "LESH_towFromFront"] call LESH_towing_fnc_getConfigValue;
_targetCustomDirection = [_target, "LESH_customDirection"] call LESH_towing_fnc_getConfigValue;

_tower_EHkilledIdx = _tower addEventHandler ["Killed", "[(_this select 0)] call LESH_towing_fnc_detach"];
_target_EHkilledIdx = _target addEventHandler ["Killed", "[((_this select 0) getVariable[""LESH_towedBy"", objNull])] call LESH_towing_fnc_detach"];

_tower setVariable ["LESH_towing", true, true];
_tower setVariable ["LESH_towTarget", _target, true];
_target setVariable ["LESH_towedBy", _tower, true];

_tower setVariable ["LESH_tower_killedEHidx", _tower_EHkilledIdx];
_target setVariable ["LESH_target_killedEHidx", _target_EHkilledIdx];

if ( !isNil "_targetCustomDirection" ) then {
	_target setVariable ["LESH_tcd", _targetCustomDirection];
};

_target setVariable ["LESH_taao", _target_axis_offset];

// move the wheel axis twice as far to make it look better
_point_distance = abs ((_target_axis_offset select 1) - (_target_wheel_offset select 1));
if (_targetTowFromFront == 1) then {
	_point_distance = _point_distance * -1;
};
_target_wheel_offset set [1, (_target_wheel_offset select 1) + _point_distance];

_target setVariable ["LESH_two", _target_wheel_offset];
_target setVariable ["LESH_ttff", _targetTowFromFront];

_tower setVariable ["LESH_toao", _tower_axis_offset];
_tower setVariable ["LESH_limiter", 0];

_instance = floor(random 1000);
_tower setVariable["LESH_instance", _instance];

_eventId = [format["LESH_towing_%1", _instance], "onEachFrame", "LESH_towing_fnc_towAction", [_tower, _target]] call BIS_fnc_addStackedEventHandler;

_speedLimiterEnabled = _tower getVariable ["LESH_limiter", 0];
if ( _speedLimiterEnabled == 0) then {
	[_tower, _instance] remoteExecCall ["LESH_towing_fnc_startSpeedLimiterOnClient", 2, false];
};

true;
