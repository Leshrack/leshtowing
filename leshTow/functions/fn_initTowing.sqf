/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_initTowing

    FEATURE
    Initiates or stops the towing action

    USAGE
    [] call LESH_towing_fnc_initTowing

    PARAMETERS
    
*/

private ["_tower", "_isTowing", "_canTow", "_target"];

_tower = [player] call LESH_towing_fnc_getTower;

if ( _tower == objNull ) exitWith { false };

_isTowing = [_tower] call LESH_towing_fnc_checkTowing;
_isBeingTowed = [(vehicle player)] call LESH_towing_fnc_checkBeingTowed;

if ( _isTowing ) then {
    [_tower] remoteExecCall ["LESH_towing_fnc_detachTowOnClient", 2, false];
    systemChat format["Detached"];
} else {
    if (_isBeingTowed) then {
        [(vehicle player)] remoteExecCall ["LESH_towing_fnc_detachTowOnClient", 2, false];
    } else {
        _canTow = [_tower] call LESH_towing_fnc_checkTowable;
        _target = _tower getVariable ["LESH_towTarget", objNull];
        
        if ( isNull _target ) exitWith { false };
        
        if ( _canTow ) then {
            systemChat format["Attached"];
            [_tower, _target] remoteExecCall ["LESH_towing_fnc_executeTowOnClient", 2, false];
        };
    };
};

true