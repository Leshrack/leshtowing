/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_showTowPoint

    FEATURE
    Creates a temporary visual aid to target vehicle

    USAGE
    [_t] call LESH_towing_fnc_showTowPoint

    PARAMETERS
    1. Object - the vehicle on which we want to display the towpoint        | MANDATORY
    1. String - Type of the vehicle (tower, target)     | MANDATORY
*/

params [["_unit", objNull],["_type", ""]];

private ["_unit_axis_offset", "_ball"];

_unit_axis_offset = [_unit, _type] call LESH_towing_fnc_getAxisOffset;
_ball = createVehicle ["Sign_Sphere10cm_F", [0,0,0], [], 0, "NONE"];
_ball attachTo [_unit, _unit_axis_offset];

[_ball] spawn {
    params ["_ball"];
    sleep 7.5;
    detach _ball;
    deleteVehicle _ball;
};

true