/*
    Author: Leshrack
    Last modified: 20180610
    Function: LESH_towing_fnc_debug

    FEATURE
    Adds visual balls to the target to debug the towpoints

    USAGE
    [_unit] call LESH_towing_fnc_debug

    PARAMETERS
    1. Object - the vehicle on which we want to display the towpoint        | MANDATORY
*/

params [["_unit", objNull]];

private ["_unit_axis_offset", "_balls", "_ball", "_unit_wheel_offset", "_canBeTowed", "_canTow"];

_balls = [];

_canTow = [_unit, "LESH_canTow"] call LESH_towing_fnc_getConfigValue;

if (_canTow == 1) then {
    _unit_axis_offset = [_unit, "tower"] call LESH_towing_fnc_getAxisOffset;
    _ball = createVehicle ["Sign_Pointer_Green_F", [0,0,0], [], 0, "NONE"];
    _ball attachTo [_unit, _unit_axis_offset];
    _balls pushBack _ball;
};

 _canBeTowed = [_unit, "LESH_canBeTowed"] call LESH_towing_fnc_getConfigValue;

if (_canBeTowed == 1) then {
    _unit_axis_offset = [_unit, "target"] call LESH_towing_fnc_getAxisOffset;
    _ball = createVehicle ["Sign_Sphere10cm_F", [0,0,0], [], 0, "NONE"];
    _ball attachTo [_unit, _unit_axis_offset];
    _balls pushBack _ball;

    _unit_wheel_offset = [_unit] call LESH_towing_fnc_getWheelOffset;
    _unit_wheel_offset set [2, 5];
    _ball = createVehicle ["Sign_Sphere10cm_F", [0,0,0], [], 0, "NONE"];
    _ball attachTo [_unit, _unit_wheel_offset];
    _balls pushBack _ball;

    _ball = createVehicle ["Sign_Arrow_Large_Green_F", [0,0,0], [], 0, "NONE"];
    _ball attachTo [_unit, [0,0,7]];
    _balls pushBack _ball;
};

_unit setVariable ["LESH_debugballs", _balls];

true;
