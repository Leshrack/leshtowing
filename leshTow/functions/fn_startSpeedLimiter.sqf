/*
    Author: Leshrack
    Last modified: 20180610
    Function: LESH_towing_fnc_startSpeedLimiter

    FEATURE

    USAGE
    [] call LESH_towing_fnc_startSpeedLimiter;


    PARAMETERS

*/

params [["_object", objNull], ["_instance", 0]];

private ["_event", "_enabled"];

if ( ! local _object ) exitWith { false };
if ( _object == objNull ) exitWith { false };

_enabled = _tower getVariable ["LESH_limiter", 0];

if (_enabled == 1) exitWith { false; };

_object setVariable ["LESH_limiter", 1];
_event = [format["LESH_speedLimiter_%1", _instance], "onEachFrame", "LESH_towing_fnc_speedLimiter", [_object]] call BIS_fnc_addStackedEventHandler;

true;
