/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_detachTowOnClient

    FEATURE
    Stops the towing action on target client machine

    USAGE
    [_tower] call LESH_towing_fnc_detachTowOnClient

    PARAMETERS
    1. Object - the towing vehicle      | MANDATORY

*/

params [["_tower", objNull]];
if ( !isServer ) exitWith { false };
if (isNull _tower) exitWith { false };

_target = _tower getVariable ["LESH_towTarget", objNull];
_clientId = owner _target;
[_tower] remoteExecCall ["LESH_towing_fnc_detach", _clientId, false];

true;