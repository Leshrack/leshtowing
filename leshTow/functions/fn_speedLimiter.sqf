/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_speedLimiter

    FEATURE
    Limits the speed of the target vehicle in order to avoid weird behaviour in towing action

    USAGE
    [_unit] call LESH_towing_fnc_speedLimiter

    PARAMETERS
    1. Object - the vehicle on which we want to apply the speed limit       | MANDATORY
    
*/

params [["_unit", objNull]];

private ["_unitSpeed", "_unitVelocity"];

if ( ! local _object ) exitWith { 
    [_object] call LESH_towing_fnc_stopSpeedLimiter;
};

_unitSpeed = speed _unit;

if ( _unitSpeed >  15 ) then {
    _unitVelocity = velocityModelSpace _unit;
    _unit setVelocityModelSpace [_unitVelocity select 0, 15 / 3.6, _unitVelocity select 2];
};
if ( _unitSpeed < - 15 ) then {
    _unitVelocity = velocityModelSpace _unit;
    _unit setVelocityModelSpace [_unitVelocity select 0, -15 / 3.6, _unitVelocity select 2];
};

true;