/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_checkTowable

    FEATURE
    Checks whether or not the towing vehicle is inposition infront/behind a towable vehicle and boundary conditions in order to initiate towing

    USAGE
    [_tower] call LESH_towing_fnc_checkTowable;

    PARAMETERS
    1. Object - the Towing vehicle      | MANDATORY

*/

params [["_tower", objNull]];

private ["_target","_canTow","_tower_axis_offset","_towPos","_objects","_minimumDistance","_closestObject","_canBeTowed","_target_axis_offset","_targetPos","_distance"];

//initiate some variables
_target = objNull;

//no valid tower found, no point in continuing
if( isNull _tower ) exitWith { false };

//lets check whether the towing vehicle can tow things according to the config
_canTow = [_tower, "LESH_canTow"] call LESH_towing_fnc_getConfigValue;

if ( isNil "_canTow" ) exitWith { systemChat format["Vehicle can't tow"]; false };
if ( _canTow != 1 ) exitWith { systemChat format["Vehicle can't tow"]; false };

//lets see if the tower is already busy towing something
if ( [_tower] call LESH_towing_fnc_checkTowing ) exitWith { systemChat format["Vehicle is already towing something"]; false };

//so the player has a towing vehicle that isn't already towing something, lets get the point for which we will start looking for towing targets
_tower_axis_offset = [_tower, "tower"] call LESH_towing_fnc_getAxisOffset;

//get the world position for the tow object
_towPos = _tower modelToWorld _tower_axis_offset;

//find suitable objects nearby
_objects = nearestObjects [_towPos, ["Air", "LandVehicle", "Ship", "thingX"], 30];

//loop through objects to check if their connection point is close enough to the _towPos
if ( (count _objects) > 0 ) then {
    _minimumDistance = 1000;
    _closestObject = objNull;
    {
        _target = _x;
        if ( _target != _tower ) then {
            _canBeTowed = [_target, "LESH_canBeTowed"] call LESH_towing_fnc_getConfigValue;
            if( ! isNil "_canBeTowed" ) then {
                if ( _canBeTowed == 1 ) then {
                    _target_axis_offset = [_target, "target"] call LESH_towing_fnc_getAxisOffset;

                    //get the world position for the towPoint
                    _targetPos = _target modelToWorld ( _target_axis_offset);

                    //calculate distance between tower and target tow positions
                    _distance = _towPos distance _targetPos;

                    //if closest found target so far set it as closest
                    if ( _distance < _minimumDistance ) then {
                        _minimumDistance = _distance;
                        _closestObject = _target;
                    };
                };
            };
        };
    } forEach _objects;

    _target = _closestObject;

    //check if we found a target
    if ( isNull _target ) exitWith { systemChat format["No suitable vehicles nearby to tow"]; false};

    //lets see if the target is not a burning wreck, although might change this at some point
    if ( !(alive _target) ) exitWith { systemChat format["Can't tow a destroyed vehicle"]; false };

    //if closes target is already getting towed
    _towed = _target getVariable ["LESH_towedBy", objNull];
    if ( !isNull _towed ) exitWith { systemChat format["Target vehicle is already towed by something"]; false };
    
    //if target still too far exit, and lets spawn some helpful markers to help position the vehicle 
    if ( _minimumDistance > 1 ) exitWith { 
        [_tower, "tower"] call LESH_towing_fnc_showTowPoint;
        [_target, "target"] call LESH_towing_fnc_showTowPoint;
        systemChat format["Target vehicle not in range"];
        false
    };

    // If vehicle doesn't contain player attempt to change locality else ignore and let the towing script run on owner locality
    if ( count ( crew _target) == 0 && ( ! (local _target) ) ) exitWith {
        //lets swap locality of the target vehicle
        [_target, player] remoteExecCall ["LESH_towing_fnc_swapOwner", 2, false];
        systemChat format["Transferring locality try again in a few seconds"];
        false;
    };

    //we got to the end of all the checks, towing can commence lets set a variable to remember what the taget vehicle was
    _tower setVariable["LESH_towTarget", _target, true];

    //and return true so that the towing function can be called
    if (true) exitWith { true };
} else {
    //no objects nearby i guess we give up
    if (true) exitWith { systemChat format["No targets nearby"]; false };
};
