/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_getConfigValue

    FEATURE
    Retrieves a config value from vehicle type. Config values can be overriden with same name variables set with setVariable to the object

    USAGE
    [_vehicle, _configProperty] call LESH_towing_fnc_getConfigValue

    PARAMETERS
    1. Object - the vehicle from which we want to extract the config value      | MANDATORY
    2. String - The name of the config property to extract  | MANDATORY

*/

params [["_unit", objNull], ["_configName", ""]];

private ["_value", "_overrideValue"];

_value = (configfile >> "CfgVehicles" >> typeOf _unit >> _configName) call BIS_fnc_getCfgData;
_overrideValue = _unit getVariable [_configName, nil];
if ( ! isNil "_overrideValue" ) then {
    _value =  _unit getVariable [_configName, nil];
};

_value