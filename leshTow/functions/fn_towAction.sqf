/*
    Author: Leshrack
    Last modified: 20180610
    Function: LESH_towing_fnc_towAction

    FEATURE
    Does towing between tower and towed vehicle;

    USAGE
    [TowVehicle, TowedVehicle] call LESH_towing_fnc_towAction

    PARAMETERS
    1. Object - Towing vehicle      | MANDATORY
    2. Object - Towable vehicle     | MANDATORY

*/

params [["_tower", objNull], ["_target", objNull]];

private ["_towPos","_targetPos", "_target_wheel_offset", "_wheelTargetPos","_distance","_direction","_speed","_partial_speed","_targetVelocity","_targetTooClose","_velocityVector","_altitudeTarget","_vectorDir", "_customDirection", "_towedFromFront","_speed_direction", "_delta_speed", "_target_forward_speed", "_target_forward_velocity"];

_target_wheel_offset = _target getVariable ["LESH_two", false];
_towPos = _tower modelToWorldVisual (_tower getVariable["LESH_toao", false]);
_targetPos = _target modelToWorldVisual (_target getVariable ["LESH_taao", false]);
_wheelTargetPos = _target modelToWorldVisual _target_wheel_offset;
_towedFromFront = _target getVariable ["LESH_ttff", 1];

//distance too large, lets detach (using 3d distance for this in case of planes falling of an edge)
if ( (_towPos distance _targetPos) > 4 ) exitWith {
    [_tower] call LESH_towing_fnc_detach;
};

// locality has changed, stop the towing because it is pointless (script commands require locality)
if ( ! local _target ) exitWith {
    [_tower] call LESH_towing_fnc_detach;
    [_tower, _target] remoteExecCall ["LESH_towing_fnc_executeTowOnClient", 2, false];
};

//the direction between the point at the rear axis on the target and the towing position on the towing vehicle
_direction = _wheelTargetPos getDir _towPos;

//if the target is towed from the back, we need to reverse the direction
if ( _towedFromFront != 1 ) then {
    _direction = _direction + 180;
};

//calculate a speed for the target to close the distance between tow points
_speed_direction = _direction - (direction _tower);

//the forward speed the target needs in relation to the towing vehicle
_partial_speed = (speed _tower) * (cos(_speed_direction));

//get the direction between the 2 tow points and normalize it to a range from -90 to 90.
//if targetTooClose is larger than 0 it means the target tow point is inbetween the tower tow point and the tow vehicle center point
_targetTooClose = _direction - (_targetPos getDir _towPos);
while { _targetTooClose < -180 } do { _targetTooClose = _targetTooClose + 360; };
while { _targetTooClose > 180 } do { _targetTooClose = _targetTooClose - 360; };
_targetTooClose = abs(_targetTooClose);
_targetTooClose = _targetTooClose - 90;

_delta_speed = (_towPos distance2D _targetPos) * 10 min 10;
_target_forward_speed = 0;

if ( _targetTooClose > 0) then {
    _target_forward_speed = _partial_speed - _delta_speed;
} else {
    _target_forward_speed = _partial_speed + _delta_speed;
};

_target_forward_velocity = (_target_forward_speed * 1.1) / 3.6;
_vectorDir = [(sin _direction), (cos _direction), 0];
_velocityVector = _vectorDir vectorMultiply _target_forward_velocity;

_altitudeTarget = (position _target) select 2;
if ( _altitudeTarget > 0.3 ) then {
    _velocityVector set [2, -1];
} else {
    _velocityVector set [2, -0.01];
};

_target setVelocity _velocityVector;
[_target, _vectorDir, _target_wheel_offset] call LESH_towing_fnc_setVectorDirE;
_target setVelocity _velocityVector;

true;
