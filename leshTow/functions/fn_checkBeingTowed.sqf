/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_checkBeingTowed

    FEATURE
    Checks if the current vehicle is being towed

    USAGE
    [_tower] call LESH_towing_fnc_checkTowing;

    PARAMETERS
    1. Object - the vehicle being towed     | MANDATORY

*/

params [["_object", objNull]];

if ( isNull _object ) exitWith { false };

if ( ! local _object ) exitWith { false };

if ( vehicle player == player) exitWith { false };

if ( isNull (_object getVariable ["LESH_towedBy", objNull])) exitWith { false };

true;
