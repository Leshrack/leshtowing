/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_executeTowOnClient

    FEATURE
    Starts the towing action on target client machine

    USAGE
    [_tower, _target] call LESH_towing_fnc_executeTowOnClient

    PARAMETERS
    1. Object - the towing vehicle      | MANDATORY
    2. Object - the target vehicle to be towed  | MANDATORY

*/

params [["_tower", objNull], ["_target", objNull]];
if ( !isServer ) exitWith { false };
if ( isNull _tower || isNull _target ) exitWith { false };

_clientId = owner _target;

[_tower, _target] remoteExecCall ["LESH_towing_fnc_tow", _clientId, false];


true;