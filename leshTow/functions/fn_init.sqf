/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_init

    FEATURE
    Initiates CBA keybind to allow players to call the towing action

    USAGE
    

    PARAMETERS
    
*/
#include "\a3\editor_f\Data\Scripts\dikCodes.h"

["Lesh's Tow mod", "attach_detach", "Attach / Detach", {_this call LESH_towing_fnc_initTowing}, {}, [DIK_B, [true, false, false]]] call CBA_fnc_addKeybind;
["Lesh's Tow mod", "speedlimiter", "Toggle Speed limiter", {_this call LESH_towing_fnc_toggleSpeedLimiter}, {}, [DIK_B, [true, true, false]]] call CBA_fnc_addKeybind;