/*
    Author: Leshrack
    Last modified: 20180602
    Function: LESH_towing_fnc_getAxisOffset

    FEATURE
    Retrieves the offset of the rear axis from the configs based on vehicle type

    USAGE
    [_vehicle] call LESH_towing_fnc_getAxisOffset

    PARAMETERS
    1. Object - the vehicle from which we want to extract the config value      | MANDATORY
    2. String - The type of object we want to request the axisoffset from       | MANDATORY

*/

params [["_unit", objNull],"_type"];

private ["_unit_axis_offset"];

switch ( _type ) do {
    case "target":
    {
        _unit_axis_offset = [_unit, "LESH_axisOffsetTarget"] call LESH_towing_fnc_getConfigValue;
    };
    case "tower":
    {
        _unit_axis_offset = [_unit, "LESH_axisOffsetTower"] call LESH_towing_fnc_getConfigValue;
    };
    default
    {
        _unit_axis_offset = [0,0,0];
    };
};

if ( isNil "_unit_axis_offset" ) exitWith { [0,0,0] };

_unit_axis_offset