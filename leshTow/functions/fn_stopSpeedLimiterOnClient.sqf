/*
    Author: Leshrack
    Last modified: 20180610
    Function: LESH_towing_fnc_stopSpeedLimiterOnClient

    FEATURE
    Stops the towing action on target client machine

    USAGE
    [_object] call LESH_towing_fnc_stopSpeedLimiterOnClient

    PARAMETERS
    1. Object - the towing vehicle      | MANDATORY

*/

params [["_object", objNull], ["_instance", 0]];

if ( !isServer ) exitWith { false };
if (isNull _object) exitWith { false };

_clientId = owner _object;

[_object, _instance] remoteExecCall ["LESH_towing_fnc_stopSpeedLimiter", _clientId, false];

true;
