/*
    Author: Leshrack
    Last modified: 20151103
    Function: LESH_towing_fnc_getWheelOffset

    FEATURE
    Retrieves a config value from vehicle type

    USAGE
    [_vehicle] call LESH_towing_fnc_getWheelOffset

    PARAMETERS
    1. Object - the vehicle from which we want to extract the wheel offset      | MANDATORY
*/

params [["_unit", objNull]];

private ["_unit_wheel_offset"];

_unit_wheel_offset = [_unit, "LESH_WheelOffset"] call LESH_towing_fnc_getConfigValue;

if( isNil "_unit_wheel_offset" ) exitWith { [0,0] };

_unit_wheel_offset;
