/*
    Author: Leshrack
    Last modified: 20180610
    Function: LESH_towing_fnc_startSpeedLimiterOnClient

    FEATURE
    Stops the towing action on target client machine

    USAGE
    [_tower] call LESH_towing_fnc_startSpeedLimiterOnClient

    PARAMETERS
    1. Object - the towing vehicle      | MANDATORY

*/

params [["_object", objNull], ["_instance", 0]];

private ["_clientId"];

if ( !isServer ) exitWith { false };
if (isNull _object) exitWith { false };

_clientId = owner _object;

[_object, _instance] remoteExecCall ["LESH_towing_fnc_startSpeedLimiter", _clientId, false];

true;
