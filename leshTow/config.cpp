class CfgPatches 
{
	class lesh_towing
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = { "CBA_Main" };
	};
};

class CfgFunctions 
{
	class LESH_towing
	{
		class functions
		{
			file = "\leshTow\functions";

			class init{postInit = 1;};
			
			class checkTowable{};
			
			class showTowPoint{};
			
			class checkTowing{};
			class checkBeingTowed{};

			class getTower{};

			class initTowing{};
			class tow{};
			class towAction{};
			class setVectorDirE{};
			class detach{};
			
			class executeTowOnClient{};
			class detachTowOnClient{};
			
			class unsetVariables{};
			
			class getConfigValue{};
			class getAxisOffset{};
			class getWheelOffset{};

			class swapOwner{};

			class debug{};
			class removeDebug{};

			class speedLimiter{};
			class toggleSpeedLimiter{};
			class startSpeedLimiter{};
			class startSpeedLimiterOnClient{};
			class stopSpeedLimiter{};
			class stopSpeedLimiterOnClient{};
		};
	};
};

class CfgRemoteExec
{
	class Functions
	{
		class LESH_towing_fnc_swapOwner {allowedTargets = 2;};
		class LESH_towing_fnc_detachTowOnClient {allowedTargets = 2;};
		class LESH_towing_fnc_executeTowOnClient {allowedTargets = 2;};
		class LESH_towing_fnc_startSpeedLimiterOnClient {allowedTargets = 2;};
		class LESH_towing_fnc_stopSpeedLimiterOnClient {allowedTargets = 2;};
	};
};