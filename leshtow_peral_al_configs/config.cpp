class CfgPatches 
{
	class lesh_tow_peral_al
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"Peral_Airfield_Logistics"};
	};
};

class CfgVehicles 	
{	
	/*extern*/ class Car;
	class Peral_B_600 : Car
	{
		LESH_canTow = 1;
		LESH_AxisOffsetTower[] = {0.26, -1.2, 1};
	};

	class Peral_AS32A : Car
	{
		LESH_canTow = 1;
		LESH_AxisOffsetTower[] = {0, -2, 1};
	};
};