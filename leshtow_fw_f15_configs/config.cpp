class CfgPatches 
{
	class lesh_tow_fwf15
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"FIR_F15_F"};
	};
};

class CfgVehicles
{
	/*extern*/ class Plane_Fighter_03_base_F;	
	class FIR_F15_Base : Plane_Fighter_03_base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9.5,-0.94};
		LESH_WheelOffset[] = {0,-2.5};
	};

	class FIR_F15D_Base : Plane_Fighter_03_base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9.5,-0.94};
		LESH_WheelOffset[] = {0,-2.5};
	};


	class FIR_F15E_Base : Plane_Fighter_03_base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9.5,-0.94};
		LESH_WheelOffset[] = {0,-2.5};
	};
};