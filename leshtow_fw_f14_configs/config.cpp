class CfgPatches 
{
	class lesh_tow_fwf14 
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"FIR_F14D_F"};
	};
};

class CfgVehicles 	
{
	/*extern*/ class Plane_Fighter_03_base_F;
	class FIR_F14D_Base : Plane_Fighter_03_base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9.5,-1.3};
		LESH_WheelOffset[] = {0,-2.5};
	};
};
