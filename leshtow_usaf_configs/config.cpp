class CfgPatches 
{
	class lesh_tow_usaf
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"USAF_A10","LDL_ac130","USAF_AC130U","usaf_b1b","USAF_B2","usaf","USAF_C130J","USAF_C17","usaf_c5","USAF_CV22","USAF_CV22IDWS","USAF_E3","USAF_F35A","usaf_f22","USAF_F16","USAF_helos","usaf_kc135","USAF_MC130"};
	};
};

class CfgVehicles 	
{	
	/*extern*/ class Plane;
	/*extern*/ class Plane_Base_F;
	/*extern*/ class Helicopter;

	class USAF_A10 : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7.5,-1.14};
		LESH_WheelOffset[] = {0,-0.5};
	};

	class USAF_AC130U_base : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,13.5,-4.7};
		LESH_WheelOffset[] = {0,-0.5};
	};

	class usaf_b1b : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,20,-2};
		LESH_WheelOffset[] = {0,-6};
	};
	
	class usaf_b2 : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,5.5,-1.91};
		LESH_WheelOffset[] = {0,-6};
	};
	
	class USAF_C130J_Base : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,15,-4.7};
		LESH_WheelOffset[] = {0,1};
	};
	
	class USAF_C17 : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,22,-2.35};
		LESH_WheelOffset[] = {0,-1};
	};
	
	class usaf_c5 : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,24,-4};
		LESH_WheelOffset[] = {0,-7};
	};
	
	class USAF_CV22 : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,8,-2.31};
		LESH_WheelOffset[] = {0,-1.5};
	};
	
	class USAF_CV22IDWS : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,8,-2.31};
		LESH_WheelOffset[] = {0,-1.5};
	};	
	
	class USAF_E3 : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,16,-0.395};
		LESH_WheelOffset[] = {0,-2};
	};
	
	class USAF_F35A : Plane_Base_F
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,6.5,-1.02};
		LESH_WheelOffset[] = {0,-3};
	};
	
	class usaf_f22 : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9.5,-1.5};
		LESH_WheelOffset[] = {0,-2};
	};
	
	class USAF_F16 : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,9,0.8};
		LESH_WheelOffset[] = {0,0};
	};

	class USAF_HH60G : Helicopter
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8.5,-1};
		LESH_WheelOffset[] = {0,3};
	};

	class USAF_HH60GMED : Helicopter
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 0;
		LESH_AxisOffsetTarget[] = {0,-8.5,-1.75};
		LESH_WheelOffset[] = {0,3};
	};

	class usaf_kc135 : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,16,-0.395};
		LESH_WheelOffset[] = {0,-2};
	};

	class USAF_MC130_Base : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,26,-4.63};
		LESH_WheelOffset[] = {0,12};
	};
};

/*

USAF_A10 - "Plane"
LDL_C130J - "USAF_AC130U","USAF_AC130U_base","Plane"
USAF_AC130U - "USAF_AC130U_base","Plane"
usaf_b1b - "Plane"
usaf_b2 - "Plane"
USAF_C130J - "USAF_C130J_Base","Plane"
USAF_C130J_Cargo - "USAF_C130J","USAF_C130J_Base","Plane"
USAF_C17 - "Plane"
usaf_c5 - "Plane"
USAF_CV22 - "Plane_Base_F","Plane"
USAF_CV22IDWS - "Plane_Base_F","Plane"
USAF_E3 - "Plane"
USAF_F35A - "Plane_Base_F","Plane"
usaf_f22 - "Plane"
USAF_F16 - "Plane"
USAF_HH60G - "Helicopter"
USAF_HH60GMED - "Helicopter"
usaf_kc135 - "Plane"
USAF_MC130 - "USAF_MC130_Base","Plane"

*/