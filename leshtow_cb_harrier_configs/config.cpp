class CfgPatches 
{
	class lesh_tow_cbharrier 
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"Cha_AV8B"};
	};
};

class CfgVehicles 	
{
	/*extern*/ class Plane;
	class Cha_AV8B_Base : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,7,-1.9};
		LESH_WheelOffset[] = {0,-1};
	};
};
