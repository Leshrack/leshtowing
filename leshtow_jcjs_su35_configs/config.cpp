class CfgPatches 
{
	class lesh_tow_jsjcsu35
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = {"JS_JC_SU35"};
	};
};

class CfgVehicles 	
{
	/*extern*/ class Plane;
	class JS_JC_SU35 : Plane
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,11,-1.4};
		LESH_WheelOffset[] = {0,-3.2};
	};
};