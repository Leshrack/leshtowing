class CfgPatches 
{
	class lesh_tow_test
	{
		units[] = { };
		weapons[] = { };
		requiredVersion = 0.100000;
		requiredAddons[] = { };
	};
};

class CfgVehicles 	
{
	/*extern*/ class Car_F;
	class Offroad_01_base_F : Car_F
	{
		LESH_canTow = 1;
		LESH_AxisOffsetTower[] = {0,-3.3,-0.55};
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,3,-0.55};
		LESH_WheelOffset[] = {0,-2};
	};

	/*extern*/ class thingX;
	class Land_MobileLandingPlatform_01_F : thingX
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_customDirection = 90;
		LESH_AxisOffsetTarget[] = {-3,0,0.9};
		LESH_WheelOffset[] = {2.2,0};
	};

	/*extern*/ class Helicopter_Base_H;
	class Heli_Light_01_base_F : Helicopter_Base_H
	{
		LESH_canBeTowed = 1;
		LESH_towFromFront = 1;
		LESH_AxisOffsetTarget[] = {0,3.5,0.4};
		LESH_WheelOffset[] = {0,1};
	};
};




